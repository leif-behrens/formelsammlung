import unittest
from formulary import *


class TestFormularyArea(unittest.TestCase):
    def test_areas_almost_equal(self):
        # Almost Equal, weil es mit der Built-In Funktion round() zu kleinen Abweichungen kommen kann

        # Rechteck
        self.assertAlmostEqual(20, FormularyArea.area_rectangle(5, 4), "Positive Integer", 3)
        self.assertAlmostEqual(59.76, FormularyArea.area_rectangle(12, 4.98), "Positiver Integer und positiver Float mit default-Rundung (3 Stellen hinter dem Komma)", 3)
        self.assertAlmostEqual(257.123, FormularyArea.area_rectangle(14.19, 18.12), "Positive Floats mit default-Rundung", 3)
        self.assertAlmostEqual(104.9975, FormularyArea.area_rectangle(25, 4.1999, 4), "Positive Integer und positver Float mit Rundung auf 4 Stellen hinter dem Komma", 3)

        # Quadrat
        self.assertAlmostEqual(25, FormularyArea.area_square(5), "Positiver Integer")
        self.assertAlmostEqual(114.918, FormularyArea.area_square(10.72), "Positiver Float mit default-Rundung (3 Stellen hinter dem Komma)", 3)
        self.assertAlmostEqual(222.61, FormularyArea.area_square(14.92, 2), "Positiver Float mit Rundung auf 2 Stellen hinter dem Komma", 3)

        # Parallelogramm
        self.assertAlmostEqual(56, FormularyArea.area_parallelogram(14, 4), "Positive Integer")
        self.assertAlmostEqual(49.92, FormularyArea.area_parallelogram(16, 3.12), "Positiver Integer und positiver Float mit default-Rundung", 3)
        self.assertAlmostEqual(74.749, FormularyArea.area_parallelogram(12.91, 5.79), "Positive Floats mit default-Rundung", 3)
        self.assertAlmostEqual(597.76, FormularyArea.area_parallelogram(18, 33.209, 2), "Positiver Integer und positiver Float mit Rundung auf 2 Stellen hinter dem Komma", 3)
        self.assertAlmostEqual(478.0413, FormularyArea.area_parallelogram(39.87, 11.99, 4), "Positive Floats mit Rundung auf 4 Stellen hinter dem Komma", 3)
    
        # Trapez
        self.assertAlmostEqual(50, FormularyArea.area_trapeze(4, 12, 13), 3)
        self.assertAlmostEqual(19.68, FormularyArea.area_trapeze(3, 5.12, 8), 3)
        self.assertAlmostEqual(48.657, FormularyArea.area_trapeze(7, 4.1, 9.802), 3)
        self.assertAlmostEqual(9.799, FormularyArea.area_trapeze(0.92, 12.3, 9.002), 3)
        self.assertAlmostEqual(58.1, FormularyArea.area_trapeze(5.81, 8, 12), 3)
        self.assertAlmostEqual(34.02, FormularyArea.area_trapeze(3.24, 9, 12, 2), 3)

        # Kreis
        self.assertAlmostEqual(452.389, FormularyArea.area_circle(12), 3)
        self.assertAlmostEqual(172.499, FormularyArea.area_circle(7.41), 3)
        self.assertAlmostEqual(31.006, FormularyArea.area_circle(math.pi), 3)
        
    def test_areas_raises_zero(self):
        # Rechteck
        self.assertRaises(ValueError, FormularyArea.area_rectangle, 0, 2)
        self.assertRaises(ValueError, FormularyArea.area_rectangle, 2, 0)

        # Quadrat
        self.assertRaises(ValueError, FormularyArea.area_square, 0)
        
        # Parallelogramm
        self.assertRaises(ValueError, FormularyArea.area_parallelogram, 10, 0)
        self.assertRaises(ValueError, FormularyArea.area_parallelogram, 0, 10)

        # Trapez
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 4, 4, 0)
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 4, 0, 4)
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 0, 4, 4)

        # Kreis
        self.assertRaises(ValueError, FormularyArea.area_circle, 0)
        
    def test_areas_raises_negative(self):
        # Rechteck
        self.assertRaises(ValueError, FormularyArea.area_rectangle, -4, 2)
        self.assertRaises(ValueError, FormularyArea.area_rectangle, 2, -1)

        # Quadrat
        self.assertRaises(ValueError, FormularyArea.area_square, -12)
        
        # Parallelogramm
        self.assertRaises(ValueError, FormularyArea.area_parallelogram, 10, -3)
        self.assertRaises(ValueError, FormularyArea.area_parallelogram, -3, 10)

        # Trapez
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 4, 4, -3)
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 4, -3, 4)
        self.assertRaises(ValueError, FormularyArea.area_trapeze, -3, 4, 4)

        # Kreis
        self.assertRaises(ValueError, FormularyArea.area_circle, -3)

    def test_areas_raises_datatype(self):
        # Rechteck
        self.assertRaises(ValueError, FormularyArea.area_rectangle, 2j, 2)
        self.assertRaises(ValueError, FormularyArea.area_rectangle, 2, 2j)
        self.assertRaises(ValueError, FormularyArea.area_rectangle, True, 2)
        self.assertRaises(ValueError, FormularyArea.area_rectangle, 2, True)
        self.assertRaises(ValueError, FormularyArea.area_rectangle, "1", 2)
        self.assertRaises(ValueError, FormularyArea.area_rectangle, 2, "1")

        # Quadrat
        self.assertRaises(ValueError, FormularyArea.area_square, 12j)
        self.assertRaises(ValueError, FormularyArea.area_square, "12")
        self.assertRaises(ValueError, FormularyArea.area_square, True)
        
        # Parallelogramm
        self.assertRaises(ValueError, FormularyArea.area_parallelogram, 10, 2j)
        self.assertRaises(ValueError, FormularyArea.area_parallelogram, 2j, 10)
        self.assertRaises(ValueError, FormularyArea.area_parallelogram, 10, True)
        self.assertRaises(ValueError, FormularyArea.area_parallelogram, True, 10)
        self.assertRaises(ValueError, FormularyArea.area_parallelogram, 10, "1")
        self.assertRaises(ValueError, FormularyArea.area_parallelogram, "1", 10)

        # Trapez
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 4, 4, 2j)
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 4, 2j, 4)
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 2j, 4, 4)
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 4, 4, True)
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 4, True, 4)
        self.assertRaises(ValueError, FormularyArea.area_trapeze, True, 4, 4)
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 4, 4, "1")
        self.assertRaises(ValueError, FormularyArea.area_trapeze, 4, "1", 4)
        self.assertRaises(ValueError, FormularyArea.area_trapeze, "1", 4, 4)

        # Kreis
        self.assertRaises(ValueError, FormularyArea.area_circle, 0)
        self.assertRaises(ValueError, FormularyArea.area_circle, True)
        self.assertRaises(ValueError, FormularyArea.area_circle, "1")


if __name__ == "__main__":
    unittest.main()
