"""
Formulary
author: Leif Behrens
"""

import math


class FormularyArea:
    """
    Class with static methods to calculate different areas
    """

    @staticmethod
    def area_rectangle(length: [int, float], width: [int, float], _round: int=3):   # pylint: disable=bad-whitespace
        """
        :param length: integer or float -> length of rectangel
        :param width: integer or float -> width of rectangle
        :param _round: integer -> round number of digist
        :return: area of rectangle
        """

        if type(length) not in [int, float] or type(width) not in [int, float]:  # pylint: disable=unidiomatic-typecheck
            raise ValueError("Invalid datatype")

        if length <= 0 or width <= 0:
            raise ValueError("Arguments must be > 0")

        if type(_round) is not int: # pylint: disable=unidiomatic-typecheck
            raise ValueError("Round value must be an integer")

        return round(length * width, _round)


    @staticmethod
    def area_square(length: [int, float], _round: int=3):    # pylint: disable=bad-whitespace
        """
        :param length: integer or float -> length of square
        :param _round: integer -> round number of digist
        :return: area of square
        """

        if type(length) not in [int, float]: # pylint: disable=unidiomatic-typecheck
            raise ValueError("Invalid datatype")

        if length <= 0:
            raise ValueError("length must be > 0")

        if type(_round) is not int: # pylint: disable=unidiomatic-typecheck
            raise ValueError("Round value must be an integer")

        return round(length ** 2, _round)


    @staticmethod
    def area_parallelogram(length: [int, float], distance_to_length: [int, float], _round: int=3):    # pylint: disable=bad-whitespace
        """
        :param length: integer or float -> length
        :param distance_to_length: interger or float -> distance of the parallel side to g
        :param _round: integer -> round number of digist
        :return: area of parallelogram
        """

        if type(length) not in [int, float] or type(distance_to_length) not in [int, float]:  # pylint: disable=unidiomatic-typecheck
            raise ValueError("Invalid datatype")

        if length <= 0 or distance_to_length <= 0:
            raise ValueError("Arguments must be > 0")

        if type(_round) is not int: # pylint: disable=unidiomatic-typecheck
            raise ValueError("Round value must be an integer")

        return round(length * distance_to_length, _round)


    @staticmethod
    def area_trapeze(distance_between_side_length: [int, float], \
        side_length_a: [int, float], \
        side_length_b: [int, float], _round: int=3): # pylint: disable=bad-whitespace
        """
        :param distance_between_side_length: integer or float -> distance from a to c
        :param side_length_a: integer or float -> side length
        :param side_length_b: integer or float -> side length
        :param _round: integer -> round number of digist
        :return: area of trapeze
        """

        if type(distance_between_side_length) not in [int, float]:  # pylint: disable=unidiomatic-typecheck
            raise ValueError("Invalid datatype")

        if type(side_length_a) not in [int, float]: # pylint: disable=unidiomatic-typecheck
            raise ValueError("Invalid datatype")

        if type(side_length_b) not in [int, float]: # pylint: disable=unidiomatic-typecheck
            raise ValueError("Invalid datatype")

        if distance_between_side_length <= 0 or side_length_a <= 0 or side_length_b <= 0:
            raise ValueError("Arguments must be > 0")

        if type(_round) is not int: # pylint: disable=unidiomatic-typecheck
            raise ValueError("Round value must be an integer")

        return round(0.5 * distance_between_side_length * (side_length_a + side_length_b), _round)


    @staticmethod
    def area_circle(radius: [int, float], _round: int=3):    # pylint: disable=bad-whitespace
        """
        :param radius: integer or float -> radius of the circle
        :param _round: integer -> round number of digist
        :return: area of circle
        """

        if type(radius) not in [int, float]: # pylint: disable=unidiomatic-typecheck
            raise ValueError("Invalid datatype")

        if radius <= 0:
            raise ValueError("Radius must be > 0")

        if type(_round) is not int: # pylint: disable=unidiomatic-typecheck
            raise ValueError("Round value must be an integer")

        return round(math.pi * (radius ** 2), _round)


if __name__ == "__main__":
    print(f"Area rectangle with a=10, b=20: {FormularyArea.area_rectangle(10, 20)}")
    print(f"Area square with a=15.25: {FormularyArea.area_square(15.25)}")
    print(f"Area parallelogram with g=12, h=3.5: {FormularyArea.area_parallelogram(12, 3.5)}")
    print(f"Area trapeze with h=4, a=16.12, c=12: {FormularyArea.area_trapeze(4, 16.12, 12)}")
    print(f"Area circle with r=pi: {FormularyArea.area_circle(math.pi)}")
